package com.itau.httpsweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Httpweb1Application {

	public static void main(String[] args) {
		SpringApplication.run(Httpweb1Application.class, args);
	}
}
