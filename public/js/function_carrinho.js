function add_carrinho(idProduto)
{
	let div = document.getElementById("produto_list_carrinho");
	let alerta = document.getElementById("alerta");

let obj = {
	idProduto : idProduto
}
     fetch("http://104.131.164.162:8083/carrinho/"+localStorage.getItem("id_sessao"),{
            method: "POST",
            body: JSON.stringify(obj),
             headers: {
              'Content-Type': 'application/json'
        }}).then(res=>{
         return res.json();
     }).then(res=>{
			 	div.innerHTML = "";
				alerta.innerHTML = "<div class=\"style-msg successmsg\"><div class=\"sb-msg\"><i class=\"icon-thumbs-up\"></i><strong>Produto adicionado com sucesso. Compre mais</strong></div><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>"
				window.setTimeout(function(){alerta.innerHTML=""},5000)
				busca_carrinho();
     });

}
function busca_carrinho()
{
	let div = document.getElementById("produto_list_carrinho");
	let qtd = document.getElementById("quantidade");
	let preco = document.getElementById("preco_list_carrinho");
     fetch("http://104.131.164.162:8083/carrinho/"+localStorage.getItem("id_sessao"),{
            method: "GET",
             headers: {
              'Content-Type': 'application/json'
        }}).then(res=>{
         return res.json();
     }).then(res=>{
			 console.log(res);
			 div.innerHTML = "";
			 if(res != null)
			 {
				 qtd.innerHTML = res.quantidadeProdutos;
				 preco.innerHTML = res.valorTotal.toFixed(2);
				 res.itemCarrinho.forEach(item => {
						 div.innerHTML += "<div class=\"top-cart-item clearfix\"><div class=\"top-cart-item-image\"><a href=\"javascript:void(0)\"><img src=\""+item.imagem+"\" alt=\""+item.nome+"\" /></a></div><div class=\"top-cart-item-desc\"><a href=\"javascript:void(0)\">"+item.nome+"</a><span class=\"top-cart-item-price\">R$"+item.preco.toFixed(2)+"</span><span class=\"top-cart-item-quantity\">x 1</span></div></div>"
				 });

			 }
			 else {
				 qtd.innerHTML = "0";
				 preco.innerHTML = "R$0.00";
				 div.innerHTML = "<h3>Carrinho vazio</h3>"
			 }
         console.log(res);
     });
}
function remove_carrinho(idProduto)
{
let obj = {
	idProduto : idProduto
}
     fetch("http://104.131.164.162:8083/carrinho/"+localStorage.getItem("id_sessao"),{
            method: "DELETE",
            body: JSON.stringify(obj),
             headers: {
              'Content-Type': 'application/json'
        }}).then(res=>{
        	window.location.reload();
     });

}
