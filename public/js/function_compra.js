$(document).ready(function(){
	let dados = {};
	let itens = "";
	// dados["idSessao"] = localStorage.getItem("id_sessao");
	fetch("http://104.131.164.162:8083/carrinho/" + localStorage.getItem("id_sessao"), {
		method: "GET",
	}).then(function(response){
		return response.json();
	}).then(function(response){
		response.itemCarrinho.forEach(item => {
			$('#table-cart tr:last').after("<tr class=\"cart_item\"><td class=\"cart-product-thumbnail\"><a href=\"#\"><img width=\"64\" height=\"64\" src=\"" + item.imagem + "\" alt=\"" + item.nome + "\"></a></td><td class=\"cart-product-name\"><a href=\"#\">" + item.nome + "</a></td><td class=\"cart-product-subtotal\"><span class=\"amount\">" + item.preco + "</span></td></tr>");
			$("#total-carrinho").text(response.valorTotal);
			$("#total-geral").text(response.valorTotal);
			$("#form-pagamento-valor").val(response.valorTotal);
		});
	});
});										

$("#finalizarPedido").click(function(){
	let dados = {};
	let request = {};
	let formCliente = $("#form-cliente").serializeArray();
	let formPagamento = $("#form-pagamento").serializeArray();
	let alerta = document.getElementById("alerta");

	// console.log(formCliente);
	// console.log(formPagamento);


	dados["idSessao"] = localStorage.getItem("id_sessao");
	dados["cliente"] = {};
	dados["cartao"] = {};
	for (var i = formCliente.length - 1; i >= 0; i--) {
		dados["cliente"][formCliente[i]["name"]] = formCliente[i]["value"];
	}
	for (var i = formPagamento.length - 1; i >= 0; i--) {
		dados["cartao"][formPagamento[i].name] = formPagamento[i].value;
	}

	fetch("http://104.131.164.162:8084/comprar", {
		method: "POST",
		headers: {
    		'Content-Type': 'application/json'
  		},
		body: JSON.stringify(dados)
	}).then(function(response){
		if(response.status == 200){
			localStorage.removeItem("id_sessao");
			alerta.innerHTML = "<div class=\"style-msg successmsg\"><div class=\"sb-msg\"><i class=\"icon-thumbs-up\"></i><strong>Compra aprovada.</strong></div><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>"
			window.setTimeout(function(){alerta.innerHTML="";window.location.href = "http://104.131.164.162/";},4000)			
		}
		else{
			alerta.innerHTML = "<div class=\"style-msg errormsg\"><div class=\"sb-msg\"><i class=\"icon-remove\"></i><strong>Não foi possível processar sua compra. Verifique as informações e tente novamente.</strong></div><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>"
			window.setTimeout(function(){alerta.innerHTML="";window.location.href = "http://104.131.164.162/";},4000)			
		}
	});
});